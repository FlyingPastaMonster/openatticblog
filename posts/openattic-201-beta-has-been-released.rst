.. title: openATTIC 2.0.1 beta has been released
.. slug: openattic-201-beta-has-been-released
.. date: 2015-07-21 15:28:30 UTC+01:00
.. tags: announcement, beta, opensource, release
.. category: 
.. link: 
.. description: Announcing the 2.0.1 beta release of openATTIC
.. type: text
.. author: Lenz Grimmer

After we announced our first preview of openATTIC 2 in June, our developers
have been busy! To give you an impression of what we’ve been working on, we’ve
now released version 2.0.1 as a beta release.

To recap, the major changes in openATTIC 2 when compared to the previous 1.2
release include:

* A completely new web UI: we’ve replaced the old UI (which was based on
  ExtJS) with a more modern and intuitive one developed in
  Boostrap/AngularJS. This will hopefully make it much easier to navigate the
  user interface and to get tasks accomplished with fewer clicks. We’ve also
  started to implement „wizards“, that will guide you through the steps of
  common tasks, e.g. creating a network share or a block device for VM
  storage. If you have any additional ideas or suggestions for wizards you
  would like to see, please let us know!
* The XML-RPC API has been replaced with a REST API, giving developers easy
  and full access to the openATTIC backend. This API is used by the new UI as
  well. The API recorder built into the web interface allows you to easily
  re-implement more complex storage management tasks by keeping a log of all
  API calls performed by the UI. You can then copy and integrate these into
  your own code, without having to consult the API documentation for each
  individual step.
* Ceph integration: we’ve also begun to develop support for the `Ceph
  <http://ceph.com/>`_ distributed object store and file system, adding
  support for visualizing the CRUSH map and monitoring.

Between the initial 2.0.0 preview release and today’s 2.0.1 beta version, our
developers have committed numerous usability improvements (e.g. adding input
validations) and added more tests, to make the new UI robust and easy to use.
Some notable functionality enhancements include:

* The addition of the aforementioned wizards that guide the administrator
  through common tasks.
* A Cluster Status Widget that provides a quick impression of a cluster’s
  actual health.
* A possibility to mark volumes as „protected“, to protect them against
  accidental removal.
* Implemented a better visual representation of a volume’s utilization level.
* Additional functionality around the management of snapshots and clones of
  volumes.

Please note that we’ve also decided to drop support for creating ftp/tftp
shares, as we consider ftp insecure and out of the scope of openATTIC’s key
competencies.

This release has a few rough edges and there are still a number of loose ends
that we intend to get fixed for the final release. In particular, some
features from the previous UI are still missing, and the documentation still
needs to be reviewed and updated to reflect the changes for the new web UI and
the REST API. But we wanted to take this opportunity to show that we’re still
around, making lots of progress, and to encourage you to give this beta a try.

You can download installable packages of version 2.0.1 for a number of Linux
distributions from our `download page <http://download.openattic.org/>`_. The
source code is available from `BitBucket <https://bitbucket.org/openattic>`_
(now including the `openattic-gui
<https://bitbucket.org/openattic/openattic-gui>`_ repository that contains the
new web UI).

If you would like to get in touch with us, consider joining our newly created
`openATTIC Users <https://groups.google.com/forum/#%21forum/openattic-users>`_
Group, or visit our `#openattic <irc://irc.freenode.org/#openattic>`_ channel
on `irc.freenode.net <http://webchat.freenode.net/?channels=openattic>`_.

We look forward to your feedback!
