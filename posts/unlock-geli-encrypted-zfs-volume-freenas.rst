.. title: Unlock Geli-encrypted ZFS Volume - FreeNAS
.. slug: unlock-geli-ecrypted-zfs-volume-freenas
.. date: 2016-07-13 22:46:12 UTC+02:00
.. tags: freenas, encrypt, decrypt, raid, zfs
.. category: 
.. link: 
.. description: 
.. type: text
.. author: Kai Wagner

Today an old friend of mine wrote me a message and asked me if I could help him with his home nas.
His problem was that his FreeNAS system could no longer mount and decrypt his disks. He wasn't quite familiar with the commandline and storage tools, so he asked me for help. 

After 15 minutes of frustrating and testing with my first FreeBSD I found a solution. For everybody how wants toor needs to decrypt a Geli-encrypted ZFS volume on FreeNAS - here's what I did:

To decrypt the volumes - first find out which one is the geli crypted - just testing every fu**ing partition::

	geli attach -k [geli_key_file] [dev_to_unlock]

	HINT: FreeNAS key-file location /data/geli/masterkeyofdoom.key

After that you have to import the zpool::

	zpool import -> list all existing zpools

	zpool import $POOLNAME -> import the named pool from above

In our scenario it wasn't possible to mount the zfs volumes after the zpool import, because the default mountpath was wrong and the main path is read-only within FreeNAS. To change the default mount path from zfs::

	zfs set mountpoint=/mnt poolname

Afterwards we could mount all existing zfs volumes to /mnt. To mount all existing volumes at a time::

	zfs mount -a

That's it. Now we could access his data again. I learned alot and now waiting for my crate beer as a reward :-).




