.. title: Speaking about openATTIC at the SUSE Monthly Open Source Talks Webinar
.. slug: speaking-about-openattic-at-the-suse-monthly-open-source-talks-webinar
.. date: 2016-07-01 13:36:16 UTC+02:00
.. tags: ceph, event, development, suse, storage, management, webinar
.. category:
.. link:
.. description: Talking at the SUSE MOST Webinar on 2016-07-06
.. type: text
.. author: Lenz Grimmer

For your information, I'll be talking about Ceph and storage management with
openATTIC on Wednesday, July 6th at 17:00 CEST (15:00 UTC) at the `SUSE
Monthly Open Source Talks (MOST) Webinar
<https://www.suse.com/partners/most>`_.

In this webinar session, I'll be giving an overview about openATTIC, e.g.
features and architecture. I will also talk about the state of the Ceph
management and monitoring functionality and our future plans.

There will be a opportunity to ask questions or post comments during the
webinar and I'd be happy to comment on them.

An in case you can't make it to the session: the recording will be available
on demand afterwards, too.

See you there!
