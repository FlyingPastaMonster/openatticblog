.. title: Collaborating with SUSE
.. slug: collaborating-with-suse
.. date: 2016-02-12 10:18:45 UTC+01:00
.. tags: collaboration, suse, ceph, opensource
.. category:
.. link:
.. description: Thoughts on the collaboration of SUSE and openATTIC
.. type: text
.. author: Lenz Grimmer

Yesterday, we announced that `it-novum <http://www.it-novum.com/>`_ (as the
main sponsor of openATTIC) and `SUSE <http://www.suse.com/>`_ have agreed to
collaborate on the development of `openATTIC <http://openattic.org/>`_ (see
the `press release
<https://www.suse.com/company/press/2016/suse-and-it-novum-collaborate-to-enhance-storage-management-in-ceph.html>`_
for more details).

SUSE has a long history and a notable reputation as a key contributor to Linux
and open source software in general. We feel honored and proud that they
approached us about this opportunity.

Our cooperation has two main objectives, that I would like to explain in some
more detail below.

.. TEASER_END

On the one hand, SUSE will collaborate with the openATTIC development team, to
accelerate the implementation of openATTIC's `Ceph <http://ceph.com/>`_
management capabilities.

We realized that we're sharing a similar goal here: making Ceph ready for
broader adoption, by lowering the barriers for managing it. Adding support for
managing Ceph is something we had on `our roadmap already
<http://tracker.openattic.org/issues/?jql=text%20~%20%22ceph%22>`_, and the
latest builds of openATTIC already include some Ceph functionality.

One of the things that we observed was that there currently is no
comprehensive Ceph management GUI available, the configuration and
administration usually needs to be performed from the command line. openATTIC
with Ceph bridges the gap between traditional storage solutions and this new
storage paradigm, as they support storage for legacy applications as well as
for modern architectures.

SUSE's expertise and resources in this area will help us to make much faster
progress on this goal. Furthermore, SUSE intends to include openATTIC as the
management interface in a future release of their Ceph-based `SUSE Enterprise
Storage <https://www.suse.com/products/suse-enterprise-storage/>`_ solution,
to simplify the management experience.

Secondly, we are working on making openATTIC available for `SUSE Linux
Enterprise Server (SLES) <https://www.suse.com/products/server/>`_ and
`openSUSE Leap <https://en.opensuse.org/Portal:42.1>`_ in the form of
installable packages. This will give SUSE users an easy way to install and use
our storage management solution. Engineers from SUSE will assist us in making
the required changes to port openATTIC to the SUSE distribution.

Adding SUSE Linux to the list of platforms that openATTIC runs on has several
benefits, both for our users, and the project itself. One the one hand, it
gives users more choice: they can use the Linux distribution they prefer or
have standardized on for deploying openATTIC in their environment. openATTIC
(the project) will benefit from the expanded user base and ecosystem, which
will help us to extend our reach and gain new users (and hopefully new
contributors).

The collaboration and development activities between openATTIC and SUSE will
take place in the open - SUSE will contribute to the upstream openATTIC
project using the `established contribution processes and communication
channels <http://openattic.org/get-involved.html>`_. openATTIC for SUSE Linux
distributions will be available from the `openSUSE Build Service (OBS)
<https://build.opensuse.org/>`_, a public service that provides a vast
repository of free software, not only for SUSE Linux distributions.

Sticking to the "release early, release often" mantra, any new Ceph management
functionality will be made available as soon as it's ready. We will continue
to publish new releases of openATTIC every 4-5 weeks, with nightly builds in
between these releases.

For us, this cooperation with SUSE is a major milestone in the history of
openATTIC. We look forward to this collaboration and we welcome SUSE to the
openATTIC community!
