.. title: Why we chose Nikola
.. slug: why-we-chose-nikola
.. date: 2015-09-17 09:00:00 UTC+02:00
.. tags: opensource, blog 
.. link: 
.. description: Why we chose Nikola as blog software
.. type: text
.. author: Kai Wagner

Why we chose Nikola - the Static Site Generator - for our newly created blog? Because its really easy to setup and configure.

Our old blog was based on Typo3 and my opinion about Typo3 is that there is no or just a horrible documentation, the code is old and the extensions are outrageous.

So no one of the openATTIC team want to write blog posts. Thats the reason why there wasn´t that much traffic on the old blog. But let me switch back to nikola.

.. TEASER_END

Nikola is really easy to install and configure: 

Debian and Ubuntu::
	
	apt-get install nikola

or if you want to have always the newest build - use pip::

	$ virtualenv nikola
	Using base prefix '/usr'
	New python executable in nikola/bin/python3
	Also creating executable in nikola/bin/python
	Installing setuptools, pip, wheel...done.
	$ cd nikola
	$ source bin/activate
	$ pip install --upgrade "Nikola[extras]"
	...snip...
	Successfully installed Nikola 

And thats it. You Successfully installed Nikola. You only have to setup nginx or apache configuration file and you could start. 

We chose apache2 for our blog. Thats our conf file - /etc/apache2/sites-available/openatticblog.conf::

	<VirtualHost *:80>
	        ServerName blog.openattic.org
	
	        DocumentRoot /var/www/html/openatticblog/output/
	        AddDefaultCharset UTF-8
	
	        CustomLog ${APACHE_LOG_DIR}/blog.openattic.org.access.log combined
	
	</VirtualHost>

Now you can use your newly created blog based on nikola.

We combined our mecurial and jenkins with the new blog for easier posting - i will explain how we have done that in my next blog post :-). 

If you need further information for getting started, please take a look at the nikola documentation at `getnikola.com <https://getnikola.com/getting-started.html>`_.	

