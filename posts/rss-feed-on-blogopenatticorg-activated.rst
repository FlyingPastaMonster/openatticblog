.. title: RSS Feed on blog.openattic.org activated
.. slug: rss-feed-on-blogopenatticorg-activated
.. date: 2016-07-08 12:22:05 UTC+02:00
.. tags: blog, rss, feed, irc, notification 
.. category: 
.. link: 
.. description: 
.. type: text
.. author: Kai Wagner

Today, I enabled the RSS feed on the blog.

You can subscribe to that feed via `https://blog.openattic.org/rss.xml
<https://blog.openattic.org/rss.xml>`_.

Moreover, every time a new post gets created and published on our blog, there
will be a notification in our IRC channel #openattic on freenode, too.
