.. title: Upcoming talks/presentations about openATTIC
.. slug: upcoming-talkspresentations-about-openattic
.. date: 2016-06-07 10:12:06 UTC+02:00
.. tags: ceph, community, conference, development, event, news, presentation, suse
.. category:
.. link:
.. description: Highlighting some upcoming presentations about openATTIC
.. type: text
.. author: Lenz Grimmer

Looks like June will be busy month for me: I have three talks/presentations
about openATTIC scheduled in my calendar for the coming weeks. If you would
like to learn more about the architecture of openATTIC, recent developments
and the current state of Ceph support, here are some opportunities:

* On Thursday, 23rd of June at 12:00 CEST, I'll be talking about `Ceph and
  Storage Management with openATTIC
  <https://events.opensuse.org/conference/oSC16/program/proposal/924>`_ at the
  `openSUSE Conference <https://events.opensuse.org/conference/oSC16>`_ in
  Nuremberg, Germany.
* On the same day, (June 23rd) at 1 pm EST (19:00 CEST), I'll be talking about
  the same subject during a `Ceph Tech Talk
  <http://ceph.com/ceph-tech-talks/>`_ (thanks to Patrick McGarry for having
  me!)
* On Wednesday, July 6th at 17:00 CEST, I'll be giving a presentation about
  `openATTIC: Ceph and Storage management on SUSE Linux
  <https://www.brighttalk.com/webcast/12535/208181>`_ for the `SUSE Monthly
  Open Source Talks (MOST) <https://www.suse.com/partners/most>`_.

I look forward to these opportunities to spread the word about openATTIC.
Hopefully this will help us to attract more users and obtain feedback and
comments on our general direction. See you there!
