.. title: Summary of SUSE Hackweek Activities
.. slug: summary-of-suse-hackweek-activities
.. date: 2016-07-08 10:06:53 UTC+02:00
.. tags: ceph, community, contributing, development, event, hackweek, salt, suse
.. category:
.. link: 
.. description: A summary of achievements from last week's visit to SUSE
.. type: text
.. author: Lenz Grimmer

.. image:: https://hackweek.suse.com/assets/hackweek-logo-light-23bdd7fcc1d3463dcdfe50670ebb017990e5ff0f47e9dce8b3bbd59bf18f0b13.png
   :alt: SUSE Hackweek Logo

The openATTIC team visited the `SUSE Headquarters <http://suse.com/>`_ in
Nuremberg last week, to take part in the 14th annual `SUSE Hackweek
<https://hackweek.suse.com/>`_.

For us, it was a good opportunity to meet with some of the engineers from the
`SUSE Enterprise Storage Team
<https://www.suse.com/products/suse-enterprise-storage/>`_ in person, to
further discuss implementation details of the Ceph Monitoring and Management
features in openATTIC, look at some UI prototypes and to learn more about each
other's ongoing projects and requirements.

.. TEASER_END

We made good progress in various areas and were able to refine and update many
`Jira issues <https://tracker.openattic.org/>`_ based on the feedback and
discussions. It was helpful for us to learn more about the state of the
`Salt-based Ceph management framework
<https://github.com/swiftgist/pillar-prototype>`_ and the iSCSI-Support using
the `lrbd utility <https://github.com/SUSE/lrbd>`_ that SUSE is working on and
to discuss how to best interact with these components from openATTIC.

Some noteworthy highlights and accomplishments of last week include:

* Stephan `implemented RBD management capabilities
  <https://bitbucket.org/FlyingPastaMonster/openattic-branches/branch/OP-1290>`_
  (:issue:`OP-749`, :issue:`OP-1290`) - in addition to viewing, it is now
  possible to create and delete RBDs from the openATTIC WebUI (a pull request
  is in the works).
* Sebastian (Krah) continued `working on the Ceph dashboard infrastructure
  <https://bitbucket.org/Exotelis/openattic/commits/branch/dashboard>`_,
  particularly on the functionality to add new widgets (e.g. performance
  graphs) and a grid layout that allows moving and resizing the widgets. The
  user will be able to create custom dashboards by selecting and arranging the
  graph widgets as he prefers.
* While adding and testing the RBD management functionality, a few bugs in the
  `REST API
  <https://wiki.openattic.org/display/OP/openATTIC+Ceph+REST+API+overview>`_
  were discovered (:issue:`OP-1309`, :issue:`OP-1302`, :issue:`OP-1305` and
  :issue:`OP-1133`), which Sebastian (Wagner) `resolved immediately
  <https://bitbucket.org/openattic/openattic/pull-requests/333>`_.
* Sebastian also `implemented a new REST API call
  <https://bitbucket.org/openattic/openattic/pull-requests/336/op-1308-added-config-detail-route-to/diff>`_
  to obtain the Ceph cluster's configuration (:issue:`OP-1308`)
* He also added some general improvements to the RBD REST Collection, like
  adding disk usage statistics and made a prototype of a REST Collection
  showing all Salt minions.
* Tatjana `extended the REST API
  <https://bitbucket.org/openattic/openattic/pull-requests/329>`_ to return a
  Ceph cluster's performance data in JSON format (which will be used by the
  Ceph dashboard) (:issue:`OP-1279`).
* Kai deployed a `public etherpad <https://etherpad.openattic.org>`_ for
  openattic.org, to further improve our collaboration infrastructure.
* I also got around to fixing a few minor packaging issues (:issue:`OP-1106`,
  :issue:`OP-1141`, :issue:`OP-1267`) and making the openATTIC login screen
  slightly more welcoming (:issue:`OP-1310`)
* Kai, Patrick & Laura made further progress regarding Ceph deployment using
  Salt and libraries provided by swiftgist and oms4suse [WIP]
* Patrick & Laura extended our CI system deployments based on Salt by adding
  configuration files for a Jenkins Slave [WIP]
* We also devised a new way for being productive in boring meetings: the
  `coding helmet <https://twitter.com/ImTheKai/status/748455761995513857>`_ !

We would like to thank SUSE for their hospitality and their support! This week
was a memorable and productive event for us.
