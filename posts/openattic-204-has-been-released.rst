.. title: openATTIC 2.0.4 beta has been released
.. slug: openattic-204-has-been-released
.. date: 2015-11-17 16:53:11 UTC+01:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. author: Tatjana Dehler

As mentioned in the :doc:`openATTIC 2.0.3 beta announcement <openattic-203-beta-has-been-released>` before, we try to stick to our 5-6 weeks release plan - time to publish openATTIC 2.0.4 beta!

This release was the first one based on an improved development workflow. We're now using Mercurial branches to separate ongoing development work from the rest of the code base and implemented code reviews and `pull requests <https://bitbucket.org/openattic/openattic/pull-requests/>`_ via BitBucket to coordinate the ongoing development work. It took a bit to get used to this way of collaborating, but it helped us to avoid trampling on each other's work or destabilizing the code base by accident. In addition to that, the code reviews help us to better distribute and share the knowledge of the various openATTIC subsystems between the developers.

.. TEASER_END

For this release we worked mainly on fixing bugs in the GUI and the API recorder, improving the test coverage of the E2E tests and took another step towards fully replacing the old XMLRPC API with the RESTful API. Moreover, we continued to implement further backend/API functionality to enable full DRBD support in the Web UI in an upcoming release. We also made some changes "behind the scenes" like some code cleanups, fixing end of line encodings or removing executable bits from files that don't need them. Additionally, this release comes with the new openATTIC logo that you might have spotted on other openATTIC web sites already.

Also, more content was added to the `openATTIC Documentation <http://docs.openattic.org/2.0/>`_, e.g. a quick HOWTO on `how to configure openATTIC with Ceph <http://docs.openattic.org/2.0/install_guides/oA_installation.html#how-to-set-up-oa-with-ceph>`_ and `how to contribute to openATTIC <http://docs.openattic.org/2.0/developer_docs/contribute.html>`_ as well as legibility improvements, e.g. in the `E2E GUI Test chapter <http://docs.openattic.org/2.0/developer_docs/dev_e2e.html>`_.

We would like to thank everyone who contributed to this release!

Please note that this is still a **beta version**. As usual, handle with care and make sure to create backups of your data!

Your feedback, ideas and bug reports are very welcome. If you would like to get in touch with us, consider joining our `openATTIC Users <https://groups.google.com/forum/#%21forum/openattic-users>`_ Google Group, visit our `#openattic <irc://irc.freenode.org/#openattic>`_ channel on `irc.freenode.net <http://webchat.freenode.net/?channels=openattic>`_ or leave comments below this blog post.

See the list below for a more detailed change log and further references. The OP codes in brackets refer to individual Jira issues that provide additional details on each item. You can review these on our `public Jira instance <http://tracker.openattic.org/>`_.

* Web UI: Fixed scripts created by the API Recorder (:issue:`OP-608`, :issue:`OP-657`, :issue:`OP-712`)
* Web UI: Created E2E tests for enabled/disabled storage tabs depending on the clicked volume type (:issue:`OP-690`)
* Web UI: Fixed visibility of storage tabs based on the volume type (:issue:`OP-692`)
* Web UI: Updated end to end tests in order to run with Firefox (in addition to Chromium) (:issue:`OP-729`)
* Web UI: Fixed look of dropdown list/menus, added autosizing support (:issue:`OP-656`)
* Web UI: Updated Logo. Clicking the logo now opens the Dashboard
* Web UI: Added example configuration template for the E2E tests
* Backend: Updated the DRBD module to use the RESTful API instead of the peerhost module (which uses the XML-RPC API) (:issue:`OP-586`)
* Backend: Added functionality and RESTful API test cases (Gatling) to create a filesystem on top of a DRBD connection (:issue:`OP-681`, :issue:`OP-707`)
* Installation: Removed executable bits from files that don't need it (:issue:`OP-696`)
* Installation: Fixed wrong end-of-line encodings in many files (LF instead of CR+LF) (:issue:`OP-695`)
* Installation: Updated openattic.conf httpd configuration file to support both the Apache 2.2 and 2.4 access control directives (:issue:`OP-709`)
* Documentation: Added note on basic Ceph configuration to the installation chapter (:issue:`OP-697`)
* Documentation: Added chapter about contributing and working with Mercurial, updated E2E test documentation
* Documentation: Improved the readability of the openATTIC documentation

