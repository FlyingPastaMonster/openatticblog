.. title: Status of openATTIC on Ubuntu Xenial
.. slug: status-of-openattic-on-ubuntu-xenial
.. date: 2016-12-06 12:29:15 UTC+01:00
.. tags: development, update, ubuntu, xenial
.. category: 
.. link: 
.. description: 
.. type: text
.. author: Lenz Grimmer

We have been working on porting openATTIC to Ubuntu 16.04 LTS "Xenial Xerus" for
quite some time now, and we wanted to give you a quick update on the current
status as of openATTIC version 2.0.16.

It turns out that Xenial provides a number of challenges and differences that we
needed to take into account, for example a new version of Django and the Django
REST framework, as well as some additional underlying changes.

Making all the required changes in a backwards-compatible manner and testing
them is quite time-intensive.

In a nutshell, we're not quite there yet, but we're making progress.

Some of these issues can be worked around, but the overall "out of the box
experience" still needs to be further improved.

.. TEASER_END

The following issues were unresolved when 2.0.16 was released, but have been
fixed in the meanwhile and should be included in the recent nightly builds:

* It's not possible to create any share on top of a volume (:issue:`OP-1697`)
* You have to run ``oaconfig install`` twice - otherwise oacli key is missing
  (:issue:`OP-1695`)
* Ubuntu Xenial: XML-RPCD: No JSON object could be decoded (:issue:`OP-1671`)
* Apache: ``/`` does not redirect to ``/openattic`` (:issue:`OP-1672`)
* API Page size parameter doesn't work (:issue:`OP-1727`)

As of openATTIC version 2.0.16, the following issues are still unresolved:

* ``targetcli`` cannot be installed next to openATTIC (:issue:`OP-1694`). This
  is not a critical issue, as we don't depend on ``targetcli`` for the
  configuration of iSCSI targets, but it's a useful tool for debugging problems
  in the LIO configuration.
* It's not possible to create a new host with iSCSI iqn or FC wwn
  (:issue:`OP-1696`). (This might not actually be specific to Xenial)
* ``oaconfig install`` requires lvm2 systemd services (:issue:`OP-1685`). A
  workaround for this is documented in the Jira issue.
* Internal Server Error on ``/api/pools`` cause of ZFS error (:issue:`OP-1662`).
  There is also a workaround described in the Jira issue.
* PNP4Nagios not properly configured on Xenial (:issue:`OP-1688`)
* Odd warning in apache's ``error.log`` (not reproducible) (:issue:`OP-1714`)

We'll continue to fix and improve the installation of openATTIC on Ubuntu
Xenial. If you are interested in testing it on this platform, we are eager to
get your feedback!